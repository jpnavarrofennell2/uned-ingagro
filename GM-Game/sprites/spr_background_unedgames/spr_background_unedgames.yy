{
    "id": "cda40147-ad9b-4caf-bd1d-8f207a4c62e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_unedgames",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60886f8b-8e11-4c49-adc5-4ed0b4bb29b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cda40147-ad9b-4caf-bd1d-8f207a4c62e7",
            "compositeImage": {
                "id": "a1b27303-47ea-4d63-bcb5-6296dc4936dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60886f8b-8e11-4c49-adc5-4ed0b4bb29b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "852c40cc-889b-4c68-9a3f-5cc8f5484ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60886f8b-8e11-4c49-adc5-4ed0b4bb29b1",
                    "LayerId": "52b76393-665e-4451-8d06-fdb479cf6cbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "52b76393-665e-4451-8d06-fdb479cf6cbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cda40147-ad9b-4caf-bd1d-8f207a4c62e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}