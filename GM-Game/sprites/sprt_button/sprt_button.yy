{
    "id": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprt_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 253,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb8fd47d-f824-418f-a713-c8178060e014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
            "compositeImage": {
                "id": "0252e80c-f88c-4368-a842-5915fd51d893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8fd47d-f824-418f-a713-c8178060e014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b78bc9c-e8d6-4901-95d6-78beecd699c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8fd47d-f824-418f-a713-c8178060e014",
                    "LayerId": "cffe05de-a051-4fd1-b25d-b0b9d5bb40cb"
                }
            ]
        },
        {
            "id": "350dbcc6-1fdb-4d7e-9838-2a345b0db916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
            "compositeImage": {
                "id": "8371df8d-cc5f-4617-856a-f4593f4d0f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350dbcc6-1fdb-4d7e-9838-2a345b0db916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7696d9-6825-4298-ac93-c2475ecc7698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350dbcc6-1fdb-4d7e-9838-2a345b0db916",
                    "LayerId": "cffe05de-a051-4fd1-b25d-b0b9d5bb40cb"
                }
            ]
        },
        {
            "id": "6aa22f64-6c30-4241-9882-a14d2ea1157d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
            "compositeImage": {
                "id": "253f46ab-992e-41fe-9c3a-296b3168f6d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aa22f64-6c30-4241-9882-a14d2ea1157d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9265339-eefa-416c-92b3-82cdb8765006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aa22f64-6c30-4241-9882-a14d2ea1157d",
                    "LayerId": "cffe05de-a051-4fd1-b25d-b0b9d5bb40cb"
                }
            ]
        },
        {
            "id": "b5445389-2228-4844-a72b-b34e8c70364e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
            "compositeImage": {
                "id": "b026426c-6f44-4868-a292-7aeaaab1eaf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5445389-2228-4844-a72b-b34e8c70364e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb803946-40ea-4fb1-8726-d5aea9c066aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5445389-2228-4844-a72b-b34e8c70364e",
                    "LayerId": "cffe05de-a051-4fd1-b25d-b0b9d5bb40cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cffe05de-a051-4fd1-b25d-b0b9d5bb40cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}