{
    "id": "98025d3f-1dd2-462e-bfdb-fea90db09d3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_m5",
    "eventList": [
        {
            "id": "dc17271e-b30a-4624-a1e1-567dfe4b8e70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "98025d3f-1dd2-462e-bfdb-fea90db09d3f"
        },
        {
            "id": "84a1017d-1169-4a09-ba3d-059e5c182b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "98025d3f-1dd2-462e-bfdb-fea90db09d3f"
        },
        {
            "id": "553d6dbd-f653-4a6a-8512-0d6d6918d91e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "98025d3f-1dd2-462e-bfdb-fea90db09d3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}