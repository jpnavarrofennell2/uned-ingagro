{
    "id": "dcf32008-5907-4647-924c-8de4159c5f50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_m1",
    "eventList": [
        {
            "id": "fdcc4d09-c2d9-46cc-b83d-95fa467c479f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcf32008-5907-4647-924c-8de4159c5f50"
        },
        {
            "id": "ea1fc0cf-2815-4753-b4f9-e1bae6599297",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcf32008-5907-4647-924c-8de4159c5f50"
        },
        {
            "id": "58868f6b-e021-4139-9a36-af2a628929d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dcf32008-5907-4647-924c-8de4159c5f50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}